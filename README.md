# bash template

This is a project to collect several useful bash functions within a single bash liberary. Having not much experience in bash, this should be used carefully.

```bash
# simply source this file from your bash script
. libbash.sh -c num -d -vv
```

