#!/usr/bin/env bash
### LIBBASH ###################################################################
# BASH library for colorful output
#
# Author: Daniel Nagel
# Copyright (C) 2020, Daniel Nagel
#
# Arguments:
#   h: show help
#   v: set verbosity mode, the more consecutive 'v' the higher
#   c: color_lvl - 0 none, 1 some, 2 utf8
#   d: print date
#
###############################################################################
__help_str="
BASH library for colorful output
Copyright (C) 2020, Daniel Nagel

Usage: $(basename $0) [OPTIONS]

Options:
    -h      Show this help.
    -v      Set verbosity mode (-v, -vv, -vvv, ...)
    -c      Set color lvl, 0=none, 1=some, 2=utf8 (not all fonts are supported)
    -d      Add date to printing output

"
showhelp() {
    printf "$__help_str"
}


### PARSE ARGUMENTS ###########################################################
verbosity=0
return_val=0
_set_date=false
_color_val=0
while getopts ":hvdc:" o; do
  case "${o}" in
    h)
      showhelp
      exit 0
      ;;
    v)
      (( verbosity=verbosity+1 ))
      ;;
    c)
      _color_val="$OPTARG"
      ;;
    d)
      _set_date=true
      ;;
    *)
      printf "Non valid option selected.\n"
      showhelp
      exit 1
      ;;
    esac
done


### ANSI FONTSTYLE ############################################################
# for more codes see https://en.wikipedia.org/wiki/ANSI_escape_code
if [[ $_color_val -eq 0 ]]; then
  blink=''
  bold=''
  reset=''
else
  blink='\e[5m'
  bold='\e[1m'
  reset='\e[0m'
fi

# colors
white='255'
black='0'
grey='238'
red='161'
blue='037'
orange='208'
green='148'

if [[ $_color_val -eq 2 ]]; then
  datecolor="$grey"
  defaultcolor="$grey"
else
  datecolor=""
  defaultcolor=""
fi

#######################################
# Set font color
# Arguments:
#   Number of color (0-255)
# Returns:
#   Return ansi excape code
########################################
setfc() {
  if [[ -n "$1" ]]; then
    printf "\e[38:5:%sm" "$1"
  else
    printf ""
  fi
}


#######################################
# Set background color
# Arguments:
#   Number of color (0-255)
# Returns:
#   Return ansi excape code
########################################
setbg() {
  if [[ -n "$1" ]]; then
    printf "\e[48:5:%sm" "$1"
  else
    printf ""
  fi
}


### PRINT ####################################################################
#######################################
# Generate colorful prefix depending on
# $_color_val
# Arguments:
#   string to print
#   color of string
# Returns:
#   Return colorful prefix string
#######################################
prefix() {
  if [[ "$_set_date" == true ]]; then
    local _date="$(showdate)"
  else
    local _date=""
  fi
  local _lvl="$(lvl_string $3)"

  local _color=${2:-$defaultcolor}
  if [[ -z "$_lvl" ]]; then
    local _lvlcolor=$_color
  else
    local _lvlcolor=$datecolor
  fi

  if [[ $_color_val -eq 2 ]]; then
    local _lvlstr=$(pbox "$_lvl" "$_lvlcolor" "$_color")
  else
    local _lvlstr=$_lvl
  fi
  local _datestr=$(pbox "$_date" "$datecolor" "$_lvlcolor")
  local _labelstr=$(pbox "$bold$1" "$_color")


  printf "%b%b%b%b " "$_datestr" "$_lvlstr" "$_labelstr" "$reset"
}


#######################################
# Generate colorful box depending on
# $_color_val
# Arguments:
#   string to print
#   color of box/text
#   color of leading element
# Returns:
#   Return colorful box
#######################################
pbox() {
  if [[ -z "$1" ]]; then
    printf ""
  else
    case "$_color_val" in
      "0")
        printf "[%s]" "$1"
        ;;
      "1")
	local _fc="$(setfc $2)"
        printf "[%b%b%b]" "$reset$_fc" "$1" "$reset"
        ;;
      "2")
        local _bg="$(setbg $2)"
        local _fc="$(setfc $white)"
        local _bgTwo="$(setbg $3)"
        local _fcTwo="$(setfc $2)"
        printf "%b %b %b%b" "$reset$_fc$_bg" "$1" "$reset$_fcTwo$_bgTwo" "$reset"
        ;;
    esac
  fi
}


#######################################
# Generate indent string
# Arguments:
#   Level of indent (int) default 0
# Returns:
#   Return indent string
########################################
lvl_string() {
  local _lvl=${1:-0}
  local _str=""

  if [[ $_lvl -ne 0 ]];then
    case "$_color_val" in
      "0" | "1") _str=" ↪ " ;;
      "2") _str="↪" ;;
    esac

    for ((i = 1 ; i < _lvl ; i++)); do
      _str="   $_str"
      if [[ $_color_val -eq 2 ]]; then
        _str=" $_str"
      fi
    done
  fi

  printf "$_str"
}


pheader() {
  if [[ $_set_date == true ]]; then
    local _suff_str="${bold}~~~~~~~~"
  else
    local _suff_str=""
  fi

  local _color=$defaultcolor

  local _suf=$(pbox "${bold}$_suff_str" "$_color" "$blue")
  local _str=$(pbox "${bold}$1" "$blue")
  printf "%b%b\n" "$_suf" "$_str"
}


pprint() {
    # $1 string to print
    # $2 color of symbol
    # $3 symbol
    # $4 level of indent
    prefix=$(prefix "$3" "$2" "$4")
    printf "%b%b" "$prefix" "$1"
}


perror() {
    # see pprint
    local _str=$(pprint "$1" "$2" "$3" "$4")
    printf "%b\n" "$_str" 1>&2
}


warn() {
  pprint "$1" "$orange" "!" "$2"
}


succ() {
  pprint "$1" "$green" "✔" "$2"
}


err() {
  perror "$1" "$red" "✘" "$2"
}



### HELPER FUNCTIONS #########################################################
#######################################
# Return current date in %d-%H:%M
# Arguments:
#   None
# Returns:
#   Return current date
########################################
showdate() {
  local _date
  _date=$(date +%d-%H:%M)
  printf "$_date"
}


run_command() {
    # $1 string to print
    # $2 string to execute
    # $3 level of print, default 0
    local _level=${3:-0}
    pprint "$1" "$blue" "${blink}…" "$_level"

    if [[ $verbosity != 0 ]]; then
      printf "\n$ %s\n" "$2"
      eval $2
    else
      eval $2 1>/dev/null 2>&1
    fi
    retval=$?

    # check if failed
    printf "\r"
    if [[ $retval == 0 ]]; then
      pprint "$1\n" "$green" "✔" "$_level"
    else
      perror "$1\n" "$red" "✘" "$_level"
      # print only help if output was not suppressed
      if [[ $2 != */dev/null* ]]; then
        perror "Use -v for more information.\n" "" "" "$(($_level+1))"
      fi
      return_val=$((return_val+1))
    fi
}


#######################################
# Delete given directory
# Arguments:
#   directory
#   string to print as warning
# Returns:
#   None
########################################
rmdir_ask() {
  if [ -d "$1" ]; then
    if [[ -z $2 ]]; then
      warn "Do you wish to delete:\n'$(pwd)/$1'\n"
    else
      warn "$2 Do you wish to delete:\n'$(pwd)/$1'\n"
    fi
    select yn in "Yes" "No"; do
      case $yn in
        "Yes")
          rm -fr $1;
          break
          ;;
        "No")
          exit
          ;;
        * ) echo "select 1 or 2";;
      esac
    done
  fi
}


#######################################
# Create directory checking if exists
# Arguments:
#   directory
# Returns:
#   None
########################################
mkdir_ask() {
  if [ -d "$1" ]; then
    rmdir_ask "$1" "The directory exists already."
  fi
  # create dir
  mkdir $1
}
